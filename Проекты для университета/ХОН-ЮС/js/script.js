$(document).ready(function(){
    $('.background_inner').slick({
        arrows: false,
        fade: true,
        autoplay: true,
        speed: 3000,
        autoplaySpeed: 3000,
        draggable: false
      });
      $('.gallery_inner').slick({
        dots: true,
        dotsClass: 'slick-dots',
        infinite: true,
        speed: 800,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: '<button type="button" class="slick-prev"><img src="../../icons/left_arrow.png"></button>',
        nextArrow: '<button type="button" class="slick-next"><img src="../../icons/right_arrow.png"></button>',
      });

    $('[data-modal=order]').on('click', function(){
      $('.overlay, .modal').show()
    });

    $('.modal_close').on('click', function(){
      $('.overlay, .modal').hide()
    });

    $('form').submit(function(e){
      e.preventDefault();
      $.ajax({
        type: "POST", 
        url: "mailer/smart.php",
        data: $(this).serialize()
      }).done(function(){
        $(this).find("input").val("");
        $('.overlay, .modal').hide()
        $('.overlay, .modal').show()
        $('form').trigger('reset');
      })
      return false;
    });

});