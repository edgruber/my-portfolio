$(document).ready(function(){
    $('.slider_inner').slick({
        dots: true,
        dotsClass: 'slick-dots',
        infinite: true,
        speed: 800,
        autoplay: true,
        autoplaySpeed: 5000,
        prevArrow: '<button type="button" class="slick-prev"><img src="../img/slider/prev.png"></button>',
        nextArrow: '<button type="button" class="slick-next"><img src="../img/slider/next.png"></button>',
      });
    $('.small_line_popular').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        speed: 400,
        prevArrow: '<button type="button" class="slick-prev"><img src="../img/product_line/left_arrow.png"></button>',
        nextArrow: '<button type="button" class="slick-next"><img src="../img/product_line/right_arrow.png"></button>',
    });

    $('[data-modal=login]').on('click', function(){
       $('.overlay, #login').fadeIn('slow').css('display','flex');  
    });
    $('.modal_close').on('click', function(){
        $('.overlay, #login').fadeOut('slow');
    });
    
    $('[data-modal=add_to_cart]').on('click', function(){
        $('.overlay2, #sizes').fadeIn('slow').css('display','flex');  
     });
     $('.modal_close').on('click', function(){
        $('#cart').fadeOut('slow');
    });
     $('.modal_close').on('click', function(){
         $('.overlay2, #sizes').fadeOut('slow');
      });

     $('[data-modal=add_to_cart2]').on('click', function(){
        $('.overlay1, #cart').fadeIn('slow').css('display','flex');
     });
     $('.modal_close').on('click', function(){
         $('#cart').fadeOut('slow');
     });

     //Удаление из корзины
    $('[data-modal=sizes]').on('click', function(){
        $('.overlay1, #cart').fadeIn('slow').css('display','flex');  
        $('.overlay2, #sizes').hide();
     });
     $('.modal_close').on('click', function(){
         $('#cart').fadeOut('slow');
     });

     $('.del1').on('click', function(){
        $('.prod1').fadeOut('50');
    });
    $('.del2').on('click', function(){
        $('.prod2').fadeOut('50');
    });
    //Галерея на странице товара
    $('[data-image=prod_img_1]').on('click', function(){
        $('.main_image_1').show();
        $('.main_image_2, .main_image_3').hide();
     });
     $('[data-image=prod_img_2]').on('click', function(){
        $('.main_image_2').show();
        $('.main_image_1, .main_image_3').hide();
     });
    $('[data-image=prod_img_3]').on('click', function(){
        $('.main_image_3').show();
        $('.main_image_1, .main_image_2').hide();
     });
     //Фильтр для цветов
     $('[data-color=black]').on('click', function(){
        $('.black').show();
        $('.white, .red, .blue, .yellow').hide();
     });

     $('[data-color=white]').on('click', function(){
        $('.white').show();
        $('.black, .red, .blue, .yellow').hide();
     });

     $('[data-color=red]').on('click', function(){
        $('.red').show();
        $('.white, .black, .blue, .yellow').hide();
     });

     $('[data-color=yellow]').on('click', function(){
        $('.yellow').show();
        $('.white, .red, .blue, .black').hide();
     });

     $('[data-color=blue]').on('click', function(){
        $('.blue').show();
        $('.white, .red, .black, .yellow').hide();
     });

     $('[data-color=clear]').on('click', function(){
      $('.white, .red, .blue, .yellow, .black').show();
     });
     
  });