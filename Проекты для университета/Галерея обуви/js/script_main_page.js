$('.small_line_popular').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    speed: 400,
    prevArrow: '<button type="button" class="slick-prev"><img src="img/product_line/left_arrow.png"></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="img/product_line/right_arrow.png"></button>',
});

$('[data-modal=login]').on('click', function(){
    $('.overlay, #login').fadeIn('slow').css('display','flex');  
});
$('.modal_close').on('click', function(){
    $('.overlay, #login').fadeOut('slow');
});

$('[data-modal=add_to_cart]').on('click', function(){
    $('.overlay2, #sizes').fadeIn('slow').css('display','flex');  
});
$('.modal_close').on('click', function(){
    $('#cart').fadeOut('slow');
});
$('.modal_close').on('click', function(){
    $('.overlay2, #sizes').fadeOut('slow');
});

$('[data-modal=add_to_cart2]').on('click', function(){
    $('.overlay1, #cart').fadeIn('slow').css('display','flex');
});
$('.modal_close').on('click', function(){
    $('#cart').fadeOut('slow');
});

$('[data-modal=sizes]').on('click', function(){
    $('.overlay1, #cart').fadeIn('slow').css('display','flex');  
    $('.overlay2, #sizes').hide();
});
$('.modal_close').on('click', function(){
    $('#cart').fadeOut('slow');
});